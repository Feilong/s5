Systemkern Simple Static Site Setup (S5)
========================================

S5 (Systemkern Simple Static Site Setup) facilitates the creation of websites using
only static HTML, taking away some of the pains that come with static HTML pages.

*S5* looks at your index.html finds the `<head>`, `<header>` and `<footer>` tags
and copies the content of those tags. It then replaces the content of all other 
HTML page's `<head>`, `<header>` and `<footer>` tags with the content from index.html   



Build Locally 
-------------
Run the script from your website root folder, as it looks for the index.html

from your website root folder run:
```bash
src/main/bash/s5
```

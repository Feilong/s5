##### Base on python 3.7 image
FROM alpine:latest
MAINTAINER MLReef

##### ADD files to the image
WORKDIR /
ADD src/main /s5


###### Setup Python and vergeml
WORKDIR /app
RUN echo "---------------------------------------------------------------------" && \
    echo "                 Systemkern Simple Static Site Setup                 " && \
    echo "---------------------------------------------------------------------" && \
    chmod +x /s5/*

ENV PATH /s5:$PATH



##### Add container startup script
#CMD echo "hello world"
